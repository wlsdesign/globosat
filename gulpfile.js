var gulp        = require('gulp');
// Live-reloading with Browser Sync
var browserSync = require('browser-sync').create();
// Requires the gulp-sass plugin
var sass = require('gulp-sass');
//Minifca CSS
var cleanCSS = require('gulp-clean-css');
//Renomeia Arquivo
var rename = require("gulp-rename");
//uglify
var uglify = require('gulp-uglify');
//Concat
var concat = require('gulp-concat');

// Static server
gulp.task('browserSync',function(){
    browserSync.init({
      proxy: "localhost/globosat"
    });
})

//Concat JS
 gulp.task('scripts', function() {
  return gulp.src(['src/js/jquery-2.1.4.min.js',
                   'src/js/angular.js',
                   'src/js/owl.carousel.js',
                   'src/js/libs/angular-ui-router.js'])
    .pipe(concat('app.js'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(uglify())
    .pipe(gulp.dest('src/js/'));
});

//Minifica CSS
gulp.task('minify-css', function() {
  return gulp.src('src/css/style.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('src/css/'));
});

// Converts Sass to CSS with gulp-sass
gulp.task('sass', function(){
  return gulp.src('src/sass/style.scss')
    .pipe(sass())
    .pipe(gulp.dest('src/css'))
    .pipe(browserSync.reload({stream: true}))
});



gulp.task('watch', ['browserSync', 'sass','minify-css','scripts'], function (){
  gulp.watch('src/sass/*.scss',['sass']);
});