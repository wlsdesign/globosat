'use strict';

var homeController = function ($scope, $state, $timeout) {
    var hc = this;
    
	hc.abrirModal = function(){
		angular.element('#modal').css('opacity', '1');
		$state.go('assine');
	}

    hc.init = function() {
    	function onResize() {
		    var w = window.outerWidth;
		    if(w < 481){
		    	var padding = 50;
		    }else{
		    	var padding = 0;
		    }
		    return padding;
		}
     $timeout(function() {
	   $('.owl-slide').owlCarousel({
	       margin:10,
	       nav:true,
	       dots:true, 
	       responsive:{
	           0:{
	               items:1
	           },
	           600:{
	               items:1
	           },
	           1000:{
	               items:1
	           }
	       }
	   });
	   
	   $('.owl-slide-eventos').owlCarousel({
	       margin:10,
			stagePadding: onResize(),
	       centerMode: true,
	       nav:true,
	       dots:true, 
	       responsive:{
	           0:{
	               items:1
	           },
	           481:{
	               items:2
	           },
	           600:{
	               items:4
	           }
	       }
	   });
	  },1000);
    }
}

homeController.$inject = ['$scope', '$state', '$timeout'];

app.controller('homeController', homeController);