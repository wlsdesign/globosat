'use strict';
var assineController = function($state){
	var ac = this;

	ac.fechaModal = function(){
		angular.element('#modal').css('opacity', '0');
		$state.go('home');
	}
};

assineController.$inject = ['$state'];

app.controller('assineController', assineController);