﻿app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) 
    {
        $urlRouterProvider.otherwise('/');

        $stateProvider
            // Home ========================================
            .state('home',
            {
                url: '/',
                views: {
                    "header": {
                        templateUrl: "partials/layout/header.html"
                    },
                    "viewContent": {
                        templateUrl: "partials/layout/home.html",
                        controller: "homeController as home"
                    },
                    "footer": {
                        templateUrl: "partials/layout/footer.html"
                    }
                }
            })
            .state('assine',
            {
                url: '/assine',
                views: {
                    "modalContent": {
                        templateUrl: "partials/modals/assine.html",
                        controller: "assineController as assine"
                    }
                }
            })
    }
]);