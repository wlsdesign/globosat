'use strict';
var eventosController = function($scope){
	//lista cards
	$scope.lista = [ 
		{
			id: 1,
			diaSemana: 'Dom',
			dia: '17',
			mes: 'Janeiro',
			imagem: 'src/img/cards/card01.jpg',
			tipoLuta:'Cinturão',
			categoria: 'Peso Galo',
			lutador1: 'Dillashaw',
			lutador2: 'Cruz'
		},
		{
			id: 2,
			diaSemana: 'Sab',
			dia: '30',
			mes: 'Janeiro',
			imagem: 'src/img/cards/card02.jpg',
			tipoLuta:'Luta Principal',
			categoria: 'Peso meio-pesado',
			lutador1: 'Johnson',
			lutador2: 'Bader'
		},
		{
			id: 3,
			diaSemana: 'Sab',
			dia: '06',
			mes: 'Fevereiro',
			imagem: 'src/img/cards/card03.jpg',
			tipoLuta:'Cinturão',
			categoria: 'Peso-pesado',
			lutador1: 'Werdum',
			lutador2: 'Velásquez'
		},
		{
			id: 4,
			diaSemana: 'Sab',
			dia: '27',
			mes: 'Fevereiro',
			imagem: 'src/img/cards/card04.jpg',
			tipoLuta:'Luta Principal',
			categoria: 'Peso-médio',
			lutador1: 'Silva',
			lutador2: 'Binsping'
		},
		{
			id: 5,
			diaSemana: 'Qui',
			dia: '10',
			mes: 'Dezembro',
			imagem: 'src/img/cards/card05.jpg',
			tipoLuta:'Luta Principal',
			categoria: 'Peso Palha',
			lutador1: 'Namajunas',
			lutador2: 'VanZant'
		},
		{
			id: 6,
			diaSemana: 'Sex',
			dia: '11',
			mes: 'Dezembro',
			imagem: 'src/img/cards/card06.jpg',
			tipoLuta:'Luta Principal',
			categoria: 'Peso Pena',
			lutador1: 'Edgar',
			lutador2: 'Mendes'
		},
		{
			id: 7,
			diaSemana: 'Sab',
			dia: '12',
			mes: 'Dezembro',
			imagem: 'src/img/cards/card07.jpg',
			tipoLuta:'Título Mundial',
			categoria: 'Peso-pena',
			lutador1: 'Aldo',
			lutador2: 'McGregor'
		},
		{
			id: 8,
			diaSemana: 'Sab',
			dia: '19',
			mes: 'Dezembro',
			imagem: 'src/img/cards/card08.jpg',
			tipoLuta:'Cinturão',
			categoria: 'Peso Leve',
			lutador1: 'Dos Anjos',
			lutador2: 'Cerrone'
		}
	]
};
eventosController.$inject = ['$scope'];

app.controller('eventosController', eventosController);